# musicAPI

Dépot contenant le client et le serveur d'un programme permettant de constituer une playlist au hasard à partir d'un fichier qui donne le nom d'un artiste et sa note sur 20

## Préparation

Utiliser la commande git clone https://gitlab.com/Julesdhs/musicapi
Avant de lancer le code, utiliser les commandes
pip install -r requirements.txt depuis les fichiers client ou musicapi

Un readme est disponible dans chacun des sous projets 

Architecture du projet :


```mermaid
graph TD
    A[client] -->|contient| B(main.py)
    A[client] -->|contient| C(README.md)
    A[client] -->|contient| D(requirements.txt)
    A[client] -->|contient| E(rudy.json)
    B -->|utilise|E

    F[musicapi] -->|contient| G(app.py)
    F[musicapi] -->|contient| H(README.md)
    F[musicapi] -->|contient| I(requirements.txt)
    F[musicapi] -->|contient| J(router.py)
    F[musicapi] -->|contient| K(test.py)
    F[musicapi] -->|contient| L(web_client.py)
    G -->|utilise|J
    G -->|utilise|L
```
