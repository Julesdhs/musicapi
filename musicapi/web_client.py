import requests
import random


def tests():
    r = requests.get(
        "https://www.theaudiodb.com/api/v1/json/2/search.php?s=rick%20astley")
    r2 = requests.get(
        "https://api.lyrics.ovh/v1/daft%20apunk/technologic")
    return(r.status_code == 200, r2.status_code == 200)


def lyrics(nom, titre):
    r4 = requests.get(str.format(
        "https://api.lyrics.ovh/v1/{}/{}", nom, titre))
    res4 = r4.json()
    if r4.status_code == 200:
        if 'lyrics' in res4.keys():
            return(res4['lyrics'])
    return('pas de lyrics')


def musiquerd(nom):
    r = requests.get(str.format(
        "https://www.theaudiodb.com/api/v1/json/2/search.php?s={}", nom))
    id = r.json()["artists"][0]['idArtist']
    r2 = requests.get(str.format(
        "https://theaudiodb.com/api/v1/json/2/album.php?i={}", id))
    res2 = r2.json()["album"]
    listetitres = []

    for album in res2:
        idalbum = album['idAlbum']
        r3 = requests.get(str.format(
            "https://theaudiodb.com/api/v1/json/2/track.php?m={}", idalbum))
        res3 = r3.json()['track']
        for track in res3:
            listetitres.append([track["strTrack"], track["strMusicVid"]])

    long = len(listetitres)
    num = random.randint(0, long)
    infos = listetitres[num]
    resultat = {"artist": nom,
                "title": infos[0],
                "suggested_youtube_url": infos[1],
                "lyrics": lyrics(nom, infos[0])[0:100]}
    return(resultat)


def playlistrd(fichier, taille):
    playlist = []
    nb = len(fichier)
    total = 0
    for artiste in fichier:
        total += artiste['note']
    # cette boucle détermine un artiste et une de ses chansons au hasard
    for i in range(taille):
        notetotale = 0
        k = random.randint(0, total)
        for j in range(nb):
            if k > notetotale:
                notetotale += fichier[j]['note']
            if k <= notetotale:
                playlist.append(musiquerd(fichier[j]['artiste']))
                k += 2000
    return(playlist)
