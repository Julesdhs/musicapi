# musicAPI



## Préparation

Avant de lancer le code, utiliser la commande 
pip install -r requirements.txt depuis le fichier musicapi


## Lancement

Pour faire fonctionner l'API, utiliser la commande 
python app.py depuis le fichier musicapi

## Test

Pour lancer le test qui vérifie que les API externes utilisées sont accessibles, utiliser la commande 
python test.py depuis le fichier musicapi