import unittest
from unittest import TestCase
from web_client import tests


class TestCode(TestCase):

    def test_en_ligne(self):
        """
        Cette fonction teste l'accès aux API externes utilisées
        """

        # ETANT DONNÉ
        t = tests()

        # ON TESTE
        self.assertEqual((True, True), t)


if __name__ == '__main__':
    unittest.main()
