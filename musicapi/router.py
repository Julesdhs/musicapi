from fastapi import APIRouter, Request
from web_client import tests, musiquerd, playlistrd


a = APIRouter()


@a.get("/", tags=["test"])
def testAPI():
    doublet = tests()
    return str.format(
        "test de l'API AudioDB : {0} \ntest de l'API LyricsOvh : {1}",
        doublet[0],
        doublet[1]
    )


@a.get("/random/{artist_name}")
def randomget(artist_name):
    return musiquerd(artist_name)


@a.post("/playlist/")
async def creation_utilisateur(request: Request):
    json_recu = await request.json()
    return playlistrd(json_recu['fichier'], json_recu['taille'])
