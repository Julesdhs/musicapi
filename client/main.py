import requests
import json

with open('rudy.json', 'r') as rudy:
    data = json.load(rudy)
    # taille à modifier selon le besoin :
    taille = 20
    # vérification du fonctionnement des API appelées
    print(requests.get("http://localhost:5000/").json())
    api_dest = "http://localhost:5000/playlist"
    req = requests.post(
        api_dest, json={'fichier': data, 'taille': taille})
    liste = req.json()

with open('playlist.txt', 'w') as playlist:
    playlist.write(str(liste))
